---
title: "Contacto"
lastmod: 2024-01-29T14:50:47-03:00
draft: false
---

## Hola!

Soy [@cecigomezz](https://www.instagram.com/cecigomezz/) 🙋🏻‍♀️ de 🇦🇷

📍San Jose, Costa Rica 🇨🇷

🧸Tatuando en Tattoo Shop [Maneki Ink CR](https://www.instagram.com/maneki_ink_cr/) 

💌 ceci@chillitattoo.ink


