---
title: "About"
lastmod: 2024-01-29T14:50:47-03:00
draft: false
---

## Hi!

I'm [@cecigomezz](https://www.instagram.com/cecigomezz/) 🙋🏻‍♀️ from 🇦🇷

📍San Jose, Costa Rica 🇨🇷

🧸Tattoo artist at [Maneki Ink CR](https://www.instagram.com/maneki_ink_cr/)

💌 ceci@chillitattoo.ink


